/*
 * button.h
 *
 *  Created on: Jun 4, 2021
 *      Author: User
 */

#ifndef BUTTON_H_
#define BUTTON_H_

// states for state machine
typedef enum
{
  IDLE = 0,
  DEBOUNCE,
  PRESSED,
  REPEAT,
  RELEASE
} BUTTON_STATE;

// struct for button
typedef struct
{
  BUTTON_STATE State;
  GPIO_TypeDef* GpioPort;
  uint16_t GpioPin;
  uint32_t LastTick;
  uint32_t TimerDebounce; // Fixed
  uint32_t TimerLongPress;
  uint32_t TimerRepeat;
  void(*ButtonPressed)(void);
  void(*ButtonLongPressed)(void);
  void(*ButtonRepeat)(void);
  int8_t(*ButtonRelease)(void);
} button_t; // TButton

// prototype public functions
void ButtonInitKey(button_t *self, GPIO_TypeDef* GpioPort, uint16_t GpioPin,
    uint32_t TimerDebounce, uint32_t TimerLongPress, uint32_t TimerRepeat);
void ButtonRegisterPressedCallback(button_t *self, void *Function);
void ButtonRegisterLongPressedCallback(button_t *self, void *Function);
void ButtonRegisterRepeatCallback(button_t *self, void *Function);
void ButtonRegisterReleaseCallback(button_t *self, void *Function);
void ButtonTask(button_t *self);

#endif /* BUTTON_H_ */

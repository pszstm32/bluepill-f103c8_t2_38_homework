/*
 * button.c
 *
 *  Created on: Jun 4, 2021
 *      Author: User
 */


#include "main.h"

#include "button.h"

// button init
void ButtonInitKey(button_t *self, GPIO_TypeDef* GpioPort, uint16_t GpioPin,
    uint32_t TimerDebounce, uint32_t TimerLongPress, uint32_t TimerRepeat)
{
  self->GpioPort = GpioPort;
  self->GpioPin = GpioPin;
  self->TimerDebounce = TimerDebounce;
  self->State = IDLE;
  self->TimerLongPress = TimerLongPress;
  self->TimerRepeat = TimerRepeat;

  // make sure is safe value
  self->ButtonPressed = NULL;
  self->ButtonLongPressed = NULL;
  self->ButtonRepeat = NULL;
  self->ButtonRelease = NULL;
}

// register callbacks
void ButtonRegisterPressedCallback(button_t *self, void *Function)
{
  self->ButtonPressed = Function;
}

void ButtonRegisterLongPressedCallback(button_t *self, void *Function)
{
  self->ButtonLongPressed = Function;
}

void ButtonRegisterRepeatCallback(button_t *self, void *Function)
{
  self->ButtonRepeat = Function;
}

void ButtonRegisterReleaseCallback(button_t *self, void *Function)
{
  self->ButtonRelease = Function;
}

// states routine
void ButtonIdleRoutine(button_t *self)
{
  if(HAL_GPIO_ReadPin(self->GpioPort, self->GpioPin) == GPIO_PIN_RESET)
  {
    self->State = DEBOUNCE;
    self->LastTick = HAL_GetTick();
  }
}

void ButtonDebounceRoutine(button_t *self)
{
  if( HAL_GetTick() - self->LastTick >  self->TimerDebounce)
  {
    if(HAL_GPIO_ReadPin(self->GpioPort, self->GpioPin) == GPIO_PIN_RESET)
    {
      self->State = PRESSED;
      if(self->ButtonPressed != NULL)
      {
        self->ButtonPressed();  // run callback
      }
      self->LastTick = HAL_GetTick();
    }
    else
    {
      self->State = IDLE;
    }
  }
}

void ButtonPressedRoutine(button_t *self)
{
  if(HAL_GPIO_ReadPin(self->GpioPort, self->GpioPin) != GPIO_PIN_RESET)
  {
    self->State = RELEASE;
  }
  else
  {
    if( HAL_GetTick() - self->LastTick >  self->TimerLongPress)
    {
      self->State = REPEAT;
      if(self->ButtonLongPressed != NULL)
      {
        self->ButtonLongPressed();
      }
      self->LastTick = HAL_GetTick();
    }
  }
}

void ButtonRepeatRoutine(button_t *self)
{
  if(HAL_GPIO_ReadPin(self->GpioPort, self->GpioPin) == GPIO_PIN_RESET)
  {
    if( HAL_GetTick() - self->LastTick >  self->TimerRepeat)
    {
      if(self->ButtonRepeat != NULL)
      {
        self->ButtonRepeat();
      }
      self->LastTick = HAL_GetTick();
    }
  }
  else
  {
    self->State = RELEASE;
  }
}

void ButtonReleaseRoutine(button_t *self)
{
  if(self->ButtonRelease != NULL)
  {
    if(self->ButtonRelease() <= 0)
    {
      self->State = IDLE;
    }
  }
  else
  {
    self->State = IDLE;
  }
}

// state machine
void ButtonTask(button_t *self)
{
  switch (self->State)
  {
    // Call procedure for current state
    case IDLE:
      ButtonIdleRoutine(self);
      break;
    case DEBOUNCE:
      ButtonDebounceRoutine(self);
      break;
    case PRESSED:
      ButtonPressedRoutine(self);
      break;
    case REPEAT:
      ButtonRepeatRoutine(self);
      break;
    case RELEASE:
      ButtonReleaseRoutine(self);
      break;
    default:
      while(1){} // Unkown state, endless loop
      break;
  }
}


